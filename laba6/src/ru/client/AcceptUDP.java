package ru.client;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class AcceptUDP {

    int port;
    int portTCP;
    InetAddress inetAddress;

    public AcceptUDP(int port) {
        this.port=port;
    }

    public void accept() {

    System.out.println("Receiver is running");
    try    {
        DatagramSocket ds = new DatagramSocket(port);
        boolean flag = true;
        while (flag)        {
            DatagramPacket pack = new DatagramPacket(new byte[64],  64);
            ds.receive(pack);
            String sentence = new String(pack.getData());
            InetAddress adrees = pack.getAddress();
            System.out.println(adrees);
            System.out.println("RECEIVED: " + sentence);
            String[] a = sentence.split(",");

            int connPord = Integer.parseInt(a[0]);
            System.out.println("Адреесс для соединения - " + adrees);
            System.out.println("Порт - " + connPord);

            this.portTCP = connPord;
            this.inetAddress = adrees;

            ds.close();
            flag = false;


        }
    }
    catch(Exception e) {
        System.out.println(e);
    }
}
}

