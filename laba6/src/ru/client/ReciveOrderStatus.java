package ru.client;

import java.net.DatagramPacket;
import java.net.DatagramSocket;


public class ReciveOrderStatus {
    int port;
    String gettingMesseg;

    public ReciveOrderStatus(int port) {
        this.port = port;
    }

    public void accept() {

        System.out.println("Waiting order status");

        try {
            DatagramSocket ds = new DatagramSocket(port);
            boolean flag = true;
            while (flag) {
                DatagramPacket pack = new DatagramPacket(new byte[64], 64);
                ds.receive(pack);
                String sentence = new String(pack.getData());
                System.out.println("RECEIVED: " + sentence);
                String[] a = sentence.split(",");

                gettingMesseg = a[0];

                if (gettingMesseg.equals("Заказ обработан")) {
                    ds.close();

                    flag = false;
                }

            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

}
