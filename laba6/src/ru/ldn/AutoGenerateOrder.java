package ru.ldn;

import java.util.ArrayList;
import java.util.UUID;


public class AutoGenerateOrder extends Thread {
    Order order = new Order();

    @Override
    public void run() {
        order.name = findName();
        order.surname = findSurname();
        order.patronymic = patronymic();
        order.email = email();
        order.idZakaza = UUID.randomUUID();

        Hats hats = new Hats();
        int n = (int) Math.floor(Math.random() * 10);
        order.order = new ArrayList<>();

        // Stuff[] massiv = new Stuff[n];
        for (int i = 0; i < n; i++) {
           // massiv[i] = new Hats();

            Hats dfdfd = new Hats();
            dfdfd.create();
            order.order.add(dfdfd);
        }
       // order.order = new ArrayList<>(Arrays.asList(massiv));


    }

    public String findSurname() {
        String[] myString = new String[]{"Иванов ", "Петров ", "Сидоров ", "Синицина ", "Абазян "};
        int n = (int) Math.floor(Math.random() * myString.length);
        return myString[n];
    }

    public String findName() {
        String[] myString = new String[]{"Иван ", "Петр ", "Сергей ", "Юлия ", "Армен "};
        int n = (int) Math.floor(Math.random() * myString.length);
        return myString[n];
    }

    public String patronymic() {
        String[] myString = new String[]{"Иванович ", "Петрововна ", "Сергеевич ", "Александровна ", "Хачитурянович "};
        int n = (int) Math.floor(Math.random() * myString.length);
        return myString[n];
    }

    public String email() {
        String[] myString = new String[]{"fddfdf ", "ghrth ", "xdthr ", "reegre ", "dhthrr "};
        int n = (int) Math.floor(Math.random() * myString.length);
        return myString[n];
    }

    public Order rOrder() {
        return this.order;
    }


}
