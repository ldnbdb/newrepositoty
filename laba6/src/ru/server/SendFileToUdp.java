package ru.server;


import java.io.IOException;
import java.net.*;

public class SendFileToUdp extends Thread {

        @Override
        public void run() {
            String mess = "15115,";
            byte[] buffer = mess.getBytes();
            String inetAddress = "192.168.101.255";

            //Добавляем broadcast адресс сети
            try {
                InetAddress address = InetAddress.getByName(inetAddress);

                DatagramPacket packet = new DatagramPacket(
                        buffer, buffer.length, address, 3333);

                DatagramSocket datagramSocket = new DatagramSocket();
                datagramSocket.send(packet);

                datagramSocket.close();

            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (SocketException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }
