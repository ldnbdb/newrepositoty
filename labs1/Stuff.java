package ru.ldn;

import java.util.Scanner;
import java.util.UUID;

abstract class Stuff implements ICrudAction {


    static int counter = 0;
    UUID id;
    String name;
    double price;
    String manufactorer;

    private static UUID productId() {
        return UUID.randomUUID();
    };



    private static double generatePrice() {
        int b = 100;
        double c;
        c= Math.random() * b;
        return c;
    };

    private static String addManufactorer() {
        String[] myString = new String[]{"раз", "два", "три", "четыре", "пять"};
        int n = (int)Math.floor(Math.random() * myString.length);
        return myString[n];
    };

    @Override
    public void create() {
        id = Stuff.productId();
        name = "product"+ id;
        counter++;
        price = Stuff.generatePrice();
        manufactorer =  Stuff.addManufactorer();

    }

    @Override
    public void read() {
        if(name==null) {
            System.out.println("Объект пуст");
        }
        else {
            System.out.println("Id товара" + "" + id);
            System.out.println("Название товара" + "" + name);
            System.out.println("цена товара" + "" + price);
            System.out.println("производитель товара" + "" + manufactorer);
        }
    }

    @Override
    public void update() {
        id = Stuff.productId();
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите название товара");
        name = scan.next();
        System.out.println("Введите цену товара. Формат: 123.45");
        price=scan.nextDouble();
        System.out.println("Введите производителя");
        manufactorer=scan.next();
        counter++;
        scan.close();

    }


    @Override
    public void delete() {
        counter--;
        id = null;
        name = null;
        price = 0;
        manufactorer = null;

    }



}
