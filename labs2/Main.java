package ru.ldn;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.UUID;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        int sizeMass;
        String view;

        System.out.println("Введите колличество объектов");
        Scanner scan = new Scanner(System.in);
        sizeMass = scan.nextInt();
        System.out.println("Введите вид представления");
        view = scan.next();
        System.out.println("размерность " + "- " + sizeMass);
        System.out.println("Вид товара " + "- " + view + "\n");


        //Создаем массив товаров с введенными параметрами
        ArrayList<Stuff> allShoesMass;
        CreateMass createMass = new CreateMass();
        allShoesMass = createMass.createMass(sizeMass, view);

        //Добавляем товары в корзину
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.add(allShoesMass.get(1));
        shoppingCart.add(allShoesMass.get(2));

        //Обновляем список заказов
        allShoesMass = shoppingCart.sendShopCard();

        //Ищем товар по id
        UUID uuid = shoppingCart.idCard.first();
        shoppingCart.serchElementFromId(uuid);


        //Добавляем пользователя и приерепляем к нему заказ
        Credentials credentials = new Credentials("Иванов", "Иван", "Иванович", "hfhdf.mail.ru");
        Order order = new Order();
        order.addOrder(credentials);
        order.addCards(allShoesMass);

        //Проверка таймера по истечении времени обработки заказов
        //Thread.sleep(6000);

        //Формируем заказ
        Orders orders = new Orders();
        orders.checkout(order);
        orders.showAllOrders();
        orders.deliteOrders();
        orders.showAllOrders();

    }
}