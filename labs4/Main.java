package ru.ldn;

import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
import java.util.UUID;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        int sizeMass;
        String view;

        System.out.println("Введите колличество объектов");
        Scanner scan = new Scanner(System.in);
        sizeMass = scan.nextInt();
        System.out.println("Введите вид представления");
        view = scan.next();
        System.out.println("размерность " + "- " + sizeMass);
        System.out.println("Вид товара " + "- " + view + "\n");


        //Создаем массив товаров с введенными параметрами
        ArrayList<Stuff> allShoesMass;
        CreateMass createMass = new CreateMass();
        allShoesMass = createMass.createMass(sizeMass, view);

        //Добавляем товары в корзину
        ShoppingCart<Stuff, UUID> shoppingCart = new ShoppingCart<>();
        shoppingCart.add(allShoesMass.get(1));
        shoppingCart.add(allShoesMass.get(2));

        //Обновляем список заказов
        allShoesMass = shoppingCart.sendShopCard();

        //Ищем товар по id
        /*UUID uuid = shoppingCart.idCard.first();
        shoppingCart.serchElementFromId(uuid);*/


        //Добавляем пользователя и приерепляем к нему заказ
        Credentials credentials = new Credentials("Петров", "Иван", "Иванович", "hfhdf.mail.ru");
        Order order = new Order();
        order.addOrder(credentials);
        order.addCards(allShoesMass);

        //Формируем заказ
        Orders<Order, Date> orders = new Orders();
        orders.checkout(order);


        //Запускаем генерацию в отдельных потоках
        AutoGenerateOrder autoGenerateOrder = new AutoGenerateOrder();
        AutoGenerateOrder autoGenerateOrder1 = new AutoGenerateOrder();
        autoGenerateOrder.start();
        autoGenerateOrder1.start();
        autoGenerateOrder.join();
        autoGenerateOrder1.join();

        orders.checkout(autoGenerateOrder.rOrder());
        orders.checkout(autoGenerateOrder1.rOrder());

        orders.showAllOrders();

        //Меняем заказ на обработан в новом потоке
        ChekingWaitOrders chekingWaitOrders = new ChekingWaitOrders();
        chekingWaitOrders.changeStatus(orders);
        chekingWaitOrders.start();
        chekingWaitOrders.join();

        orders.showAllOrders();


        ChekingProcessedOrder chekingProcessedOrder = new ChekingProcessedOrder();
        chekingProcessedOrder.changeStatus(orders);
        chekingProcessedOrder.start();
        chekingProcessedOrder.join();

        orders.showAllOrders();


    }
}