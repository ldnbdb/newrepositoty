package ru.ldn;

import java.util.*;

public class Orders <T1 extends Order, T2>  {

    List<T1> checkout = new LinkedList<>() ;
    Map<T2, T1> hashMap = new HashMap<>();

    public synchronized void checkout (T1 a) {

        checkout.add(a);

        Date data = a.dateNow;
        hashMap.put((T2) data, a);

    }

    void deliteOrders() {
        for (T1 c: checkout) {
            if (c.zakaz.equals("Обработан")) {
                checkout.remove(c);
            }
        }

    }

    void showAllOrders () {
        if (checkout.isEmpty()) {
            System.out.println("Список заказов пуст");
        }
        else {
            for (T1 c : checkout) {
                System.out.println("Заказ c товарами " + c.order.toString());
                System.out.println("Владельцем заказа является: " + c.surname + c.name + c.patronymic);
                System.out.println("Статус заказа " + c.zakaz);

            }
        }
    }


}
