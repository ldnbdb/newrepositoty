package ldn;

import java.util.ArrayList;
import java.util.Arrays;


public class CreateMass {

    ArrayList<Stuff> stuffs;


    public ArrayList<Stuff> createMass(int sizeMass, String view) {
        Stuff[] mass = new Stuff[sizeMass];


        if (view.equals("Tshirts")) {
            for (int i = 0; i < sizeMass; i++) {
                mass[i] = new Tshirts();
                mass[i].create();
            }
        } else if (view.equals("Hats")) {
            for (int i = 0; i < sizeMass; i++) {
                mass[i] = new Hats();
                mass[i].create();
            }

            System.out.println("Колличество созданных объектов " + "-" + Stuff.counter);


        } else {
            System.out.println("Данного товара не существует");
        }
        stuffs = new ArrayList<>(Arrays.asList(mass));
        return stuffs;

    }

}
