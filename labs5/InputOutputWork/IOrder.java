package ldn.InputOutputWork;


import java.util.UUID;

public interface IOrder  {

    void readById(UUID uuid);

    void saveById(UUID uuid);

    void readAll();

    void saveAll();


}
