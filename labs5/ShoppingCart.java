package ldn;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.TreeSet;


public class ShoppingCart <T1 extends Stuff, T2> implements Serializable {

    ArrayList<T1> shopCard = new ArrayList<>();
    TreeSet<T2> idCard = new TreeSet<>();


    void add(T1 a) {
        shopCard.add(a);
        idCard.add((T2) a.id);
    }

    <T3> void delite(T3 a) {
        shopCard.remove(a);
    }

    void serchElementFromId(T2 uuid) {
        for (T1 a : shopCard) {
            if (a.id == uuid) {
                System.out.println("Объект с id " + uuid + "Найден");
                System.out.println("Цена равна " + "= " + a.price);
                System.out.println("Производитель " + "- " + a.manufactorer);
                System.out.println("Название товара " + "- " + a.name);
            }
        }
    }

    void showAllElementsOfCart() {
        for (T1 a : shopCard) {
            System.out.println("Объект " + a.id);
            System.out.println("Цена равна " + "= " + a.price);
            System.out.println("Производитель " + "- " + a.manufactorer);
            System.out.println("Название товара " + "- " + a.name);
        }
    }


    ArrayList<T1> sendShopCard() {
        return this.shopCard;

    }

}

