package ru.client;

import ru.ldn.AutoGenerateOrder;
import ru.ldn.Order;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

public class SendTCP {
    public void sendTCP (InetAddress adress, int portTCP) {

        AutoGenerateOrder autoGenerateOrder2 = new AutoGenerateOrder();
        autoGenerateOrder2.run();
        try {
            autoGenerateOrder2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Order order = autoGenerateOrder2.rOrder();

        try {
            Socket socket = new Socket(adress, portTCP);
            OutputStream outputStream = socket.getOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);

            System.out.println("Client connected to socket.");

            objectOutputStream.writeObject(order);
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
