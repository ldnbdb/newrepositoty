package ru.ldn;


import java.io.*;
import java.util.List;
import java.util.UUID;

public class BiteFileClass extends AManageOrder {
    List<Order> orders;
    Order order;


    BiteFileClass(List s) {
        this.orders = s;
    }

    @Override
    public void readById(UUID uuid) {
        System.out.println("///////////Читаем из файла по id///////////");
        try {
            ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream
                    (new FileInputStream("/home/dmitry/test1.txt")));
            List<Order> ord = (List<Order>) ois.readObject();
            for (Order b : ord) {
                if (b.idZakaza == uuid) {
                    System.out.println("Заказ с id = " + b.idZakaza);
                }
            }

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveById(UUID uuid) {
        ObjectOutputStream obj = null;
        try {
            obj = new ObjectOutputStream(new BufferedOutputStream(
                    new FileOutputStream("/home/dmitry/test2.txt")));
            for (Order c : orders) {
                if (c.idZakaza == uuid) {
                    obj.writeObject(c);
                }

            }
            obj.flush();
            obj.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void readAll() {
        System.out.println("///////////Читаем из файла///////////");
        try {
            ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream
                    (new FileInputStream("/home/dmitry/test1.txt")));
            List<Order> ord = (List<Order>) ois.readObject();
            for (Order b : ord) {
                System.out.println("Заказ с id = " + b.idZakaza);
            }


        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void saveAll() {
        ObjectOutputStream out = null;

        try {
            out = new ObjectOutputStream(new BufferedOutputStream(
                    new FileOutputStream("/home/dmitry/test1.txt")));

            out.writeObject(orders);

            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}


