package ru.ldn;

import java.io.Serializable;

public class Credentials implements Serializable {



    public String surname;
    public String name;
    public String patronymic;
    public String email;



    public Credentials (String surname, String name, String patronymic, String email) {
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.email = email;


    }

}
