package ru.ldn;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

public class JSONClass extends AManageOrder {

    List<Order> orders;
    JSONObject jsonObjectWrite = new JSONObject();
    JSONArray jsonArray = new JSONArray();

    public JSONClass(List s) {
        this.orders = s;
    }

    public JSONClass() { }

    @Override
    public void readById(UUID uuid) {
        JSONParser jsonParser = new JSONParser();
        try {
            JSONObject object = (JSONObject) jsonParser.parse(
                    new FileReader("/home/dmitry/test.json"));
            JSONArray jsonArray = (JSONArray) object.get("Данные о заказах:");
            System.out.println("/////////Заказ с id " + uuid + " ///////////////");

           for (Object a: jsonArray) {
               if (a.toString().contains(uuid.toString())) {
                   System.out.println("> " + a);
               }
           }

        } catch (ParseException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void saveById(UUID uuid) {
        for (Order or : orders) {
            if (or.idZakaza == uuid) {
                jsonArray.add(" Заказ номер " + String.valueOf(or.idZakaza) + " Содержит товары -  " +
                        String.valueOf(or.order) + " Владелец заказа - " +
                        String.valueOf(or.surname) + " Почтовый адрес владельца - " +
                        String.valueOf(or.email));
            }
            jsonObjectWrite.put("Данные о заказах:", jsonArray);

            try (FileWriter writer = new FileWriter("/home/dmitry/test.json")) {
                writer.write(String.valueOf(jsonObjectWrite));
                writer.flush();
            } catch (IOException ex) {
                ex.printStackTrace();
            }


        }
    }

    @Override
    public void readAll() {
        JSONParser jsonParser = new JSONParser();
        try {
            JSONObject object = (JSONObject) jsonParser.parse(
                    new FileReader("/home/dmitry/test.json"));
            JSONArray jsonArray = (JSONArray) object.get("Данные о заказах:");
            System.out.println("///////////////Данные из файла///////////////////////");

            jsonArray.forEach(e -> System.out.println("> " + e));


        } catch (ParseException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void saveAll() {

        for (Order or : orders) {
            jsonArray.add(" Заказ номер " + String.valueOf(or.idZakaza) + " Содержит товары -  " +
                    String.valueOf(or.order) + " Владелец заказа - " +
                    String.valueOf(or.surname) + " Почтовый адрес владельца - " +
                    String.valueOf(or.email));


        }

        jsonObjectWrite.put("Данные о заказах:", jsonArray);


        try (FileWriter writer = new FileWriter("/home/dmitry/test.json")) {
            writer.write(String.valueOf(jsonObjectWrite));
            writer.flush();
        } catch (IOException ex) {
            ex.printStackTrace();
        }


    }
}
