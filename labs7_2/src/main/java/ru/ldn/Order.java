package ru.ldn;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

public class Order implements Serializable {


    public String zakaz = "В ожидании";
    public Date dateNow = new Date();
    public UUID idZakaza;


    public String surname;
    public String name;
    public String patronymic;
    public String email;
    public ArrayList<Stuff> order;

    public void addOrder(Credentials credentials) {
        this.surname = credentials.surname;
        this.name = credentials.name;
        this.patronymic = credentials.patronymic;
        this.email = credentials.email;

        idZakaza = Order.productId();
    }

    void addCards(ArrayList<Stuff> arrayList) {
        this.order = arrayList;
    }

    private static UUID productId() {
        return UUID.randomUUID();
    };


}



