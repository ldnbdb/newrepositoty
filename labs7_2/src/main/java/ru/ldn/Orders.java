package ru.ldn;

import spring.boot.MyExeption;

import java.util.*;

public class Orders <T1 extends Order, T2>  {

    public List<T1> checkout = new LinkedList<>() ;
    public Map<T2, T1> hashMap = new HashMap<>();

    public void checkout (T1 a) {
        synchronized (checkout) {
            checkout.add(a);

            Date data = a.dateNow;
            hashMap.put((T2) data, a);
        }
    }

    void deliteOrders() {
        synchronized (checkout) {
            for (T1 c : checkout) {
                if (c.zakaz.equals("Обработан")) {
                    checkout.remove(c);
                }
            }
        }
    }

    public int delById(UUID uuid) throws MyExeption {
        int a = 1;
        if (checkout.isEmpty()) throw new MyExeption("2 -Корзина пуста, удалять нечего");
        for (T1 z : checkout) {
            if (z.idZakaza.equals(uuid)) {
                checkout.remove(z);
                a = 0;
            }
        }
        if ( a== 1)  throw new MyExeption("1 - данного заказа нет");
        return a;
    }


    void changeStatus() {
        for (T1 c: checkout) {
            if (c.zakaz.equals("В ожидании")) {
                c.zakaz = "Обработан";
            }
        }

    }

    public void showAllOrders () {
        if (checkout.isEmpty()) {
            System.out.println("Список заказов пуст");
        }
        else {
            for (T1 c : checkout) {
                System.out.println("Заказ c товарами " + c.order.toString());
                System.out.println("Владельцем заказа является: " + c.surname + c.name + c.patronymic);
                System.out.println("Статус заказа " + c.zakaz);

            }
        }
    }


}
