package ru.server;

import ru.ldn.Order;
import ru.ldn.Orders;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

public class MonoThreadClientHandler implements Runnable {

    public static Socket clientDialog;
    Order orderResult;
    Orders orders;

    public MonoThreadClientHandler(Socket s, Orders _orders) {
        orders = _orders;
        MonoThreadClientHandler.clientDialog = s;
    }

    @Override
    public void run() {

        try {
            ObjectInputStream deserializer = new ObjectInputStream(clientDialog.getInputStream());

            while(!clientDialog.isClosed()) {
                System.out.println("Server reading from channel");


                try {
                    Order order = (Order) deserializer.readObject();
                    System.out.println(order.surname);
                    //  new MySerserSocet().addid(order);
                    orders.checkout(order);
                    System.out.println("Объект добавлен");
                    clientDialog.close();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
