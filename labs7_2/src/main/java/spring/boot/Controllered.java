package spring.boot;


import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.ldn.AutoGenerateOrder;

import java.util.UUID;


@RestController
public class Controllered {
    @GetMapping("/test")
    public String sdf(@RequestParam String command,
                      @RequestParam (required=false, defaultValue = "") UUID order_id,
                      @RequestParam (required=false, defaultValue = "") String card_id) throws InterruptedException {
        final Logger log = Logger.getLogger(Controllered.class);
        WorkWithCommands work = new WorkWithCommands();
        JSONArray jsonArray= null;
        if (command.equals("readAll")) {
            log.info("Список заказов выведен");
           jsonArray= work.readAll();
        }
        else if (command.equals("readById")) {
            log.info("Заказ с номером id -" + order_id + "выведен");
           jsonArray = work.readById(order_id);
        }
        else if (command.equals("addToCard")) {
            AutoGenerateOrder autoGenerateOrder = new AutoGenerateOrder();
            autoGenerateOrder.start();
            autoGenerateOrder.join();
            autoGenerateOrder.order.idZakaza= UUID.fromString(card_id);
            CreateOrders.method().checkout(autoGenerateOrder.rOrder());
            log.info("Заказ с номером id " + card_id + " добавлен" + "//////////////////////////");
            CreateOrders.orders.showAllOrders();
            return "Добавлен заказ " + card_id;
        }
        else if (command.equals("delById")) {
            int result = 0;
            try {
                result = CreateOrders.method().delById(order_id);
                log.info("заказ " + order_id + " удален");
            } catch (MyExeption myExeption) {
                myExeption.loggerWriter();
                log.error(myExeption);
            }
            return String.valueOf(result);
        }
        else {
           MyExeption exept = new MyExeption("3- не верная команда");
           exept.loggerWriter();
           log.error(exept);
           return "Не верная команда";
        }

        return jsonArray + "";
    }

}