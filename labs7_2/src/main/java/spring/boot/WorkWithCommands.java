package spring.boot;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.UUID;

public class WorkWithCommands {
    UUID id;


    public JSONArray readAll() {
        JSONParser jsonParser = new JSONParser();
        JSONObject object = null;
        JSONArray jsonArray = null;
        try {
            object = (JSONObject) jsonParser.parse(
                    new FileReader("/home/dmitry/test.json"));
            jsonArray = (JSONArray) object.get("Данные о заказах:");
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

        return jsonArray;
    }

    public JSONArray readById(UUID uuid) {
        this.id= uuid;
        System.out.println(id);
        JSONArray jsonArray = null;
        JSONParser jsonParser = new JSONParser();
        try {
            JSONObject object = (JSONObject) jsonParser.parse(
                    new FileReader("/home/dmitry/test.json"));
            jsonArray = (JSONArray) object.get("Данные о заказах:");

            for (Object a: jsonArray) {
                if (a.toString().contains(id.toString())) { ;
                    jsonArray.clear();
                    jsonArray.add(a);
                }
            }

        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }
        return jsonArray;
    }
}
